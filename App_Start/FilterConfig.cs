﻿using System.Web;
using System.Web.Mvc;

namespace reserva_laboratorios
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new Filters.VerificaSesion()); //agregamos el filtro creado
        }
    }
}
