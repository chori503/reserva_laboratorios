﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using reserva_laboratorios.Models;

namespace reserva_laboratorios.Controllers
{
    public class AccesosController : Controller
    {

        public ActionResult Index()
        {
            return View();
        }

        // GET: Accesos

        public ActionResult Ingresar()
        {
            return View();
        }
        public ActionResult Salir()
        {
            Session["Usuario"] = null;
            return RedirectToAction("Ingresar", "Accesos");
        }
        [HttpPost]
        public ActionResult Ingresar(string user, string password)
        {
            try
            {
                using (reserva_labsEntities bDatos = new reserva_labsEntities())
                {
                    var usr = (from d in bDatos.usuario
                               where d.usuario1 == user.Trim() &&
                                     d.pass == password.Trim() &&
                                     d.estado == true
                               select d).FirstOrDefault();
                    if (usr == null)
                    {
                        ViewBag.Error = "Password y user No existe";
                        return View();
                    }
                    else
                    {
                        Session["Usuario"] = usr;
                        Session["Usr"] = user;
                    }
                    return RedirectToAction("Index", "reservas");
                }
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;
                return View();
            }
        }

    }
}
