﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using reserva_laboratorios.Models;

namespace reserva_laboratorios.Controllers
{
    public class reservasController : Controller
    {
        private reserva_labsEntities db = new reserva_labsEntities();

        // GET: reservas
        public async Task<ActionResult> Index()
        {
            var reserva = db.reserva.Include(r => r.docente).Include(r => r.horario).Include(r => r.laboratorio).Include(r => r.materia);
            return View(await reserva.ToListAsync());
        }

        // GET: reservas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserva reserva = await db.reserva.FindAsync(id);
            if (reserva == null)
            {
                return HttpNotFound();
            }
            return View(reserva);
        }

        // GET: reservas/Create
        public ActionResult Create()
        {
            ViewBag.id_docente = new SelectList(db.docente, "id", "nombres");
            ViewBag.id_horario = new SelectList(db.horario, "id", "horario_Reserva");
            ViewBag.id_laboratorio = new SelectList(db.laboratorio, "id", "nombre");
            ViewBag.id_materia = new SelectList(db.materia, "id", "nombre");
            return View();
        }

        // POST: reservas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "id,id_docente,id_materia,id_laboratorio,fecha,id_horario,estado")] reserva reserva)
        {
            if (ModelState.IsValid)
            {
                db.reserva.Add(reserva);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.id_docente = new SelectList(db.docente, "id", "nombres", reserva.id_docente);
            ViewBag.id_horario = new SelectList(db.horario, "id", "id", reserva.id_horario);
            ViewBag.id_laboratorio = new SelectList(db.laboratorio, "id", "nombre", reserva.id_laboratorio);
            ViewBag.id_materia = new SelectList(db.materia, "id", "nombre", reserva.id_materia);
            return View(reserva);
        }

        // GET: reservas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserva reserva = await db.reserva.FindAsync(id);
            if (reserva == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_docente = new SelectList(db.docente, "id", "nombres", reserva.id_docente);
            ViewBag.id_horario = new SelectList(db.horario, "id", "horario_Reserva", reserva.id_horario);
            ViewBag.id_laboratorio = new SelectList(db.laboratorio, "id", "nombre", reserva.id_laboratorio);
            ViewBag.id_materia = new SelectList(db.materia, "id", "nombre", reserva.id_materia);
            return View(reserva);
        }

        // POST: reservas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "id,id_docente,id_materia,id_laboratorio,fecha,id_horario,estado")] reserva reserva)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reserva).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.id_docente = new SelectList(db.docente, "id", "nombres", reserva.id_docente);
            ViewBag.id_horario = new SelectList(db.horario, "id", "id", reserva.id_horario);
            ViewBag.id_laboratorio = new SelectList(db.laboratorio, "id", "nombre", reserva.id_laboratorio);
            ViewBag.id_materia = new SelectList(db.materia, "id", "nombre", reserva.id_materia);
            return View(reserva);
        }

        // GET: reservas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reserva reserva = await db.reserva.FindAsync(id);
            if (reserva == null)
            {
                return HttpNotFound();
            }
            return View(reserva);
        }

        // POST: reservas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            reserva reserva = await db.reserva.FindAsync(id);
            db.reserva.Remove(reserva);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
