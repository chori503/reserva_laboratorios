﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Mvc;
using reserva_laboratorios.Models;
using reserva_laboratorios.Controllers;

namespace reserva_laboratorios.Filters
{
    public class VerificaSesion : ActionFilterAttribute //heredar del ActionFilterAttribute
    {
        usuario obUsuario = new usuario();
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                base.OnActionExecuted(filterContext);
                obUsuario = (usuario)HttpContext.Current.Session["Usuario"];//Session en controller
                if (obUsuario == null)//Si la sesion es nula
                {
                    if (filterContext.Controller is AccesosController == false) // valida contolador 
                    {
                        filterContext.HttpContext.Response.Redirect("~/Accesos/Ingresar"); //envia a login
                    }
                }
            }
            catch(Exception)
            {
                filterContext.Result= new RedirectResult("~/Accesos/Ingresar"); //envia a login

            }
            }

    }
}